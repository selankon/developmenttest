# Notes

I didn't well documented both parts (backend and frontend) due lack of time.

Any questions refer to selankon@selankon.tk .

# Instructions

1. Install backend dependencies:

  ```
  pip install Django
  python -m django --version
  1.11.17
  ```
  We use 1.11 because is the lastest [LTS](https://en.wikipedia.org/wiki/Django_(web_framework)#Version_history)

  Other dependencies that it use are:

  ```
  pip install djangorestframework
  sudo pip install requests
  pip install django-cors-headers
  ```

2. Install frontend dependencies:

  ```
  npm install
  bower install
  ```

3. Run the servers:
  * For backend on:

  ```
  cd backend
  python manage.py runserver
  ```

  * For frontend

  ```
  cd frontend
  gulp serve
  ```

# Questions

1. Describe why you choose this layout.

* Django is a powerful web framework that lets you to very fast create a backend server with ORM. I used `djangorestframework` to help me on set up a REST server. As I would like to use Angular, that works mostly of times with JSON objects, i thought that is the best option. I import the models from an existing database using the command: `python manage.py inspectdb > models.py` and then modifying manually the resulting file to fit the conditions.

* Angularjs (Angular version 1) is also a powerful and amazing javascript framework. I choose it because I worked before with it and I know some good practices of programming. Angular lets you to fastly create a frontend app. I used `Yeoman` as scaffolding tool  that able me to create the application faster (configuring gulp, npm, bower... and also creating angular elements such directives with simply `yo angular:directive directive`).

2. What would you do to improve the user experience?

The frontend ui is really simple, some improvements could be:

* Style the document or use some framework such Angular Material
* Create a CRUD interface to create input reports
* Adding new features like see already matched records and change its. 

And depending of project necessities:

* Create user authentication (with JWT for example)
* Internationalization
