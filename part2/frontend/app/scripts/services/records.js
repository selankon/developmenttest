'use strict';

/**
 * @ngdoc service
 * @name frontendApp.records
 * @description
 * # records Used as model and dictionary for records object
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  // Model for records
  .value('Record',function Record(data) {
    this.id = data.id
    this.artist = data.artist
    this.title = data.title
    this.isrc = data.isrc
    this.duration = data.duration
    if (data.matched) this.matched = data.matched
  })

  // Dictionary "storage" for Records
  .factory ('recordsDict' , [ 'Record',
    function (Record) {
      var map = {};

      function getOrCreate (data){
        var record = map[data.id];
        if (!record){
          record = new Record(data);
          map[data.id] = record;
        }
        return record;
      }
      function getAll() {
        return Array.from( Object.values(map) );
      }

      return {
        map: map,
        // Generate records list dictionary from get response
        put: function (data){
          data.forEach (function (record){
            var r = getOrCreate(record);
          });
          return getAll();
        },
        get: function(id) {
          if (id) {
            var data = {};
            data.id = id
            return getOrCreate(data)
          }
          return getAll();
        },
        delete: function (inputRecord) {
          delete map[inputRecord.id];
        }
      }
    }]);
