'use strict';

/**
 * @ngdoc service
 * @name frontendApp.matches
 * @description
 * # matches
 * Service in the frontendApp.
 */
 angular.module('frontendApp')
   // Model for records
   .value('Match', function Match(data, Record) {
     this.match = data.match
     this.differences = data.differences
     this.record = new Record(data.record)
   })

   // Dictionary "storage" for Records
   .factory ('matchDict' , [ 'Match', 'Record',
     function (Match, Record) {
       var map = {};

       function getOrCreate (data){
         var match = map[data.id];
         if (!match){
           match = new Match(data, Record);
           map[data.id] = match;
         }
         return match;
       }
       function getAll() {
         return Array.from( Object.values(map) );
       }

       return {
         map: map,
         // Generate records list dictionary from get response
         put: function (data){
           data.forEach (function (match){
             var r = getOrCreate(match);
           });
           return getAll();
         },
         get: function(id) {
           if (id) {
             var data = {};
             data.id = id
             return getOrCreate(data)
           }
           return getAll();
         },
         delete: function (){
           map = {}
         }
       }
     }]);
