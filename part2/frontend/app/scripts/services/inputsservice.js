'use strict';

/**
 * @ngdoc service
 * @name frontendApp.inputs
 * @description
 * # inputs
 * Service in the frontendApp.
 */
angular.module('frontendApp')
    .service('inputsService', [ 'inputsRemote' , 'recordsDict', 'inputsRemoteDetail',
      function (inputsRemote, recordsDict, inputsRemoteDetail) {
        this.get = function (cb){
          inputsRemote.get().$promise
            .then (
              function (data){
                console.log("Received Lists!" , data );
                var r = recordsDict.put(data);
                if (cb) cb(r)
              },
              function(e) {
                console.log("Error on inputsRemote.get()");
                console.log(e);
              }
            )
        }
        this.select = function(match, inputRecord, cb){
          console.log("Selecting ", match, inputRecord);
          inputRecord.matched = match.record.id
          inputsRemoteDetail.post({pk: inputRecord.id }, inputRecord).$promise
          .then (
            function(data){
              console.log("Done.");
              if (cb) cb(data)
            }
          )
        }
        this.delete  = function(inputRecord){
          recordsDict.delete(inputRecord)
          return recordsDict.get()
        }
      }])
      .service('inputsRemote', ['$resource', 'config',
      function($resource, config){
        var base = config.BASE+config.INPUTS;
        return $resource(base, {}, {
          get: {
            method:'GET',
            isArray: true
          }

      })
      }])
      .service('inputsRemoteDetail', ['$resource', 'config',
      function($resource, config){
        var base = config.BASE+config.INPUTS;
        return $resource(base+"/:pk", {}, {
          post: {
            method:'PUT'
          }

      })
  }]);
