'use strict';

/**
 * @ngdoc service
 * @name frontendApp.matchesservice
 * @description
 * # matchesservice
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .service('matchesservice', [ 'matchesRemote' , 'matchDict',
    function (matchesRemote, matchDict) {
      this.get = function (id, cb){
        matchesRemote.get({pk: id}).$promise
          .then (
            function (data){
              console.log("Received Lists!" , data );
              matchDict.delete()
              var r = matchDict.put(data);
              if (cb) cb(r)
            },
            function(e) {
              console.log("Error on inputsRemote.get()");
              console.log(e);
            }
          )
      }

    }])
    .service('matchesRemote', ['$resource', 'config',
    function($resource, config){
      var base = config.BASE+config.MATCHES;
      return $resource(base+"/:pk", {pk: '@pk'}, {
        get: {
          method:'GET',
          isArray: true
        }
    })
  }]);
