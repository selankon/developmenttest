'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('MainCtrl',  [ '$scope', 'inputsService', 'matchesservice', '$timeout',
                function ($scope, inputsService, matchesservice, $timeout) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.matches = ""
    $scope.selected = ""
    inputsService.get(function (r) {
      $scope.inputRecords = r;
    })

    $scope.selectInput = function (input){
      $scope.selected = input
      matchesservice.get(input.id, function (r) {
          $scope.matches = r;
      })
    }

    $scope.selectMatch = function (match){
      if (confirm('Confirm that the selected song \n' +
                  $scope.selected.title + $scope.selected.artist + $scope.selected.isrc + $scope.selected.duration +
                  '\nmatch with:\n'+
                  match.record.title + match.record.artist + match.record.isrc + match.record.duration )
        ){

      }
      inputsService.select(match, $scope.selected, function (input){
        $scope.inputRecords = inputsService.delete(input)
        $scope.matches = "";
        console.log($scope.inputRecords);
      })
    }

  }]);
