'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:recordlist
 * @description
 * # recordlist
 */
angular.module('frontendApp')
  .directive('recordlist', function () {
    return {
      templateUrl: 'scripts/directives/templates/recordlist.html',
      restrict: 'A',
      scope: {
        data : '=',
        cb : '='
      }
    };
  });
