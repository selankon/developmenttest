'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:matchlist
 * @description
 * # matchlist
 */
angular.module('frontendApp')
  .directive('matchlist', function () {
    return {
      templateUrl: 'scripts/directives/templates/matchlist.html',
      restrict: 'AEC',
      scope: {
        data : '=',
        cb : '='
      }
    };
  });
