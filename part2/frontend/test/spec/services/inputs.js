'use strict';

describe('Service: inputs', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var inputs;
  beforeEach(inject(function (_inputs_) {
    inputs = _inputs_;
  }));

  it('should do something', function () {
    expect(!!inputs).toBe(true);
  });

});
