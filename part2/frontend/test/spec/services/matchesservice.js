'use strict';

describe('Service: matchesservice', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var matchesservice;
  beforeEach(inject(function (_matchesservice_) {
    matchesservice = _matchesservice_;
  }));

  it('should do something', function () {
    expect(!!matchesservice).toBe(true);
  });

});
