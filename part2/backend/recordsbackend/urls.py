# from django.urls import path
from django.conf.urls import url
from recordsbackend import views

# from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^inputs/([0-9]+)', views.InputDetail.as_view()),
    url(r'^inputs$', views.InputList.as_view()),
    url(r'^matches/([0-9]+)', views.MatchDetail.as_view()),

]
