# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

class Record(models.Model):
    artist = models.CharField(max_length=256, blank=True, null=True)
    title = models.CharField(max_length=256, blank=True, null=True)
    isrc = models.CharField(max_length=256, blank=True, null=True)
    duration = models.SmallIntegerField(blank=True, null=True)
    # id = models.AutoField(primary_key=True, serialize=False, verbose_name='id')

    class Meta:
        managed = False
        db_table = 't_sound_recordings'

class InputRecord(Record):
    matched = models.ForeignKey(Record, models.DO_NOTHING, db_column='matched', blank=True, null=True, related_name='+')

    class Meta:
        managed = False
        db_table = 't_input_report'



class Match(models.Model):
    id_records = models.ForeignKey(Record, models.DO_NOTHING, db_column='id_records', primary_key=True, related_name='record')
    id_inputs = models.ForeignKey(InputRecord, models.DO_NOTHING, db_column='id_inputs', primary_key=True, related_name='input')
    match = models.SmallIntegerField(blank=True, null=True)
    differences = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_matches'
        unique_together = (('id_records', 'id_inputs'),)
