from recordsbackend.models import Record, InputRecord, Match
from rest_framework import serializers

class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('id', 'artist', 'title', 'isrc', 'duration')

class InputRecordSerializer(RecordSerializer):
    class Meta:
        model = InputRecord
        fields = ('id', 'matched', 'artist', 'title', 'isrc', 'duration')

class MatchSerializer(serializers.ModelSerializer):
    record = RecordSerializer(source='id_records', read_only=True)
    id = serializers.CharField(source='id_records.id')
    class Meta:
        model = Match
        fields = ( 'record', 'match', 'differences', 'id')
