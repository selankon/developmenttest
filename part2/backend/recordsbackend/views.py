from recordsbackend.models import Record
from recordsbackend.serializers import RecordSerializer
from recordsbackend.models import InputRecord
from recordsbackend.serializers import InputRecordSerializer
from recordsbackend.models import Match
from recordsbackend.serializers import MatchSerializer

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class MatchDetail(APIView):
    """
    Generate a matches from records. Get posible matches from a Record
    """
    def get_matches(self, pk):
        try:
            return Match.objects.filter(
                id_inputs=pk
            ).order_by('-match')
        except Match.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        matches = self.get_matches(pk)
        serialized = []
        for match in matches:
            serialized.append(MatchSerializer(match).data)
        print serialized[0]
        return Response(serialized)


class InputList(APIView):
    def get(self, request):
        records = InputRecord.objects.exclude(matched__isnull=False)

        serializer = InputRecordSerializer(records, many=True)
        return Response(serializer.data)

class InputDetail(APIView):
    """
    Post input service to set specified Match
    """
    def get_object(self, pk):
        try:
            return InputRecord.objects.get(pk=pk)
        except InputRecord.DoesNotExist:
            raise Http404


    def put(self, request, pk):
        record = self.get_object(pk)
        print request.data
        serializer = InputRecordSerializer(record, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
