-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.1
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: sound_recordings | type: DATABASE --
-- -- DROP DATABASE IF EXISTS sound_recordings;
-- CREATE DATABASE sound_recordings;
-- -- ddl-end --
-- 

-- object: public.t_sound_recordings | type: TABLE --
-- DROP TABLE IF EXISTS public.t_sound_recordings CASCADE;
CREATE TABLE public.t_sound_recordings(
	id serial NOT NULL,
	artist character varying(256),
	title character varying(256),
	isrc character varying,
	duration smallint,
	CONSTRAINT sound_recordings_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.t_sound_recordings OWNER TO postgres;
-- ddl-end --

-- object: public.t_input_report | type: TABLE --
-- DROP TABLE IF EXISTS public.t_input_report CASCADE;
CREATE TABLE public.t_input_report(
	record_ptr_id serial NOT NULL,
	matched smallint,
	CONSTRAINT input_report_pk PRIMARY KEY (record_ptr_id)

);
-- ddl-end --
ALTER TABLE public.t_input_report OWNER TO postgres;
-- ddl-end --

-- object: public.t_matches | type: TABLE --
-- DROP TABLE IF EXISTS public.t_matches CASCADE;
CREATE TABLE public.t_matches(
	id_records smallint NOT NULL,
	id_inputs smallint NOT NULL,
	match smallint,
	differences character varying(1000),
	CONSTRAINT match_pk PRIMARY KEY (id_records,id_inputs)

);
-- ddl-end --
ALTER TABLE public.t_matches OWNER TO postgres;
-- ddl-end --

-- object: matched_fk | type: CONSTRAINT --
-- ALTER TABLE public.t_input_report DROP CONSTRAINT IF EXISTS matched_fk CASCADE;
ALTER TABLE public.t_input_report ADD CONSTRAINT matched_fk FOREIGN KEY (matched)
REFERENCES public.t_sound_recordings (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: report_fk | type: CONSTRAINT --
-- ALTER TABLE public.t_input_report DROP CONSTRAINT IF EXISTS report_fk CASCADE;
ALTER TABLE public.t_input_report ADD CONSTRAINT report_fk FOREIGN KEY (record_ptr_id)
REFERENCES public.t_sound_recordings (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: id_records_fk | type: CONSTRAINT --
-- ALTER TABLE public.t_matches DROP CONSTRAINT IF EXISTS id_records_fk CASCADE;
ALTER TABLE public.t_matches ADD CONSTRAINT id_records_fk FOREIGN KEY (id_records)
REFERENCES public.t_sound_recordings (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: id_inputs_fk | type: CONSTRAINT --
-- ALTER TABLE public.t_matches DROP CONSTRAINT IF EXISTS id_inputs_fk CASCADE;
ALTER TABLE public.t_matches ADD CONSTRAINT id_inputs_fk FOREIGN KEY (id_inputs)
REFERENCES public.t_input_report (record_ptr_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


