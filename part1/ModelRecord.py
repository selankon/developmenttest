#!/usr/bin/python

class Record(object):
    """
    Model for sound records
    """
    table = "t_sound_recordings"

    def __init__(self, dbConnection,
            id = None,
            artist = None,
            title = None,
            isrc = None,
            duration = 0):
        self.dbConnection = dbConnection
        self.id = id
        self.artist = artist
        self.title = title
        self.isrc = isrc
        if (duration == ''): # Used on .csv import with empty
            self.duration = int()
        else:
            self.duration = int(duration)


    def insert (self):
        # print("Insert ", self.__dict__)
        sql = "INSERT INTO "+Record.table+" (artist, title, isrc, duration) VALUES (%s, %s, %s, %s)"
        val = (self.artist, self.title, self.isrc, self.duration)
        self.dbConnection.query(sql, val)

    def compare(self, input):
        """
        Compare with other record giving a % int as a result and a list of differences.
        * For each field match gives 25%
        * For title and artist gives 10% if input text are contained on record text and
            viceversa. Also put the field on the list of differences.
            TODO: Implement a function that compare the similarity between words to be more acurate
        * If no matches put the compared field on the list of differences
        :return: keymap with % of matching and differences. Keys are: 'score' and 'diferences'
        """
        # print ("Comparing ",self.id, " with ", input.id)
        dif = list();
        score = 0;

        # Isrc comparasion
        if (self.isrc == input.isrc): score += 25
        else: dif.append("isrc")

        # Duration comparasion
        if (self.duration == input.duration and
        (self.duration != 0 and input.duration != 0) ): score += 25
        else: dif.append("duration")

        # Title comparaison
        if (self.title == input.title): score += 25
        elif(self.title.__contains__(input.title) or
            input.title.__contains__(self.title)):
            score += 10
            dif.append("title")
        else: dif.append("title")

        # Artist comparasion
        if (self.artist == input.artist): score += 25
        elif(self.artist.__contains__(input.artist) or
            input.artist.__contains__(self.artist)):
            score += 10
            dif.append("artist")
        else: dif.append("artist")
        return {'score' : score, 'diferences' :dif}
        # print ("Score: ", score, "with differences: ",dif)

class InputRecord(Record):
    """
    Model for input records. Inheritance from Records.
    """

    table = "t_input_report"

    def __init__(self, dbConnection,
            id = None,
            artist = None,
            title = None,
            isrc = None,
            duration = 0,
            matched = None):

        self.matched = matched
        Record.__init__(self, dbConnection,
                id,
                artist,
                title,
                isrc,
                duration)

    def insert (self):
        sql = """
        WITH new_input AS (
            INSERT INTO {t_record} ( artist, title, isrc, duration) VALUES (%s, %s, %s, %s) RETURNING id
        )
        INSERT INTO {t_input_report}(record_ptr_id)
        SELECT id FROM new_input;
        """.format(t_record = Record.table, t_input_report = InputRecord.table)
        val = (self.artist, self.title, self.isrc, self.duration)
        self.dbConnection.query(sql, val)

    @staticmethod
    def getAll (db):
        """
        :return: list of InputRecord
        """
        # sql = "select id, artist, title, isrc, duration, matched from "+InputRecord.table
        sql = """
            select id, artist, title, isrc, duration
            from {t_record}
            where id IN (select record_ptr_id from {t_input_report} )
            """.format(t_record = Record.table, t_input_report = InputRecord.table)
        db.query(sql, None)
        data = db._db_cur.fetchall()
        matchList = list()
        for i in range(len(data)):
            matchList.append(InputRecord(db,*data[i]))
        return matchList

    def getPosibleMatches(self):
        """
        From an input Record get the posible matches.
        Basically is a SQL call that compare field by field a input_record by its
        id's and get correspondents stored records that could match.
        :return: list of Records posible matches
        """

        sql = """
            WITH reports AS (select REC.*
            	from   {t_record}  REC
            	LEFT   JOIN {t_input_report} i ON REC.id = i.record_ptr_id
            	WHERE  i.record_ptr_id IS NULL)

            select rep.*
            from  reports rep
            where

            rep.isrc = (
            	select isrc from  {t_record}
            	where id=CAST({id} as integer)
            	and isrc <> '')
            OR
            rep.artist LIKE (
            	select artist from {t_record}
            	where id=CAST({id} as integer) )
            OR
            rep.title LIKE (
            	select title from {t_record}
            	where id=CAST({id} as integer) )
            OR
            rep.duration = (
            	select duration from {t_record}
            	where id=CAST({id} as integer)
            	and duration <> 0)
                """.format(id = self.id, t_record = Record.table, t_input_report = InputRecord.table)

        self.dbConnection.query(sql, None)
        data = self.dbConnection._db_cur.fetchall()
        matchList = list()
        for i in range(len(data)):
            matchList.append(Record(self.dbConnection,*data[i]))

        return matchList
