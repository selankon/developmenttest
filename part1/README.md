# Instructions

1. Install dependencies

  ```
  pip install psycopg2
  ```
2. Check postgres connection configurations on `config.py`
3. Run the script `setup.py` that automatically will:
    1. Create database or drop and create if exists
    2. Populate the database using the provided `.csv` files
    3. Populate table `t_matches` which contain the information about the possible matches and set a percentage score for each mach and a list of differences.



# Questions

1. Describe briefly the matching method chosen.

  First, using a sql query I get the possible matchers comparing each camp of the table.

  Then, using python, it compare each camp from the input record with each camp of each possible matching giving a score. As there are 4 camps, each score is a 25%. The scoring method are documented on the function comments on  [`part1/ModelRecord.py`](part1/ModelRecord.py) `Record.compare()` **line 32**.

  Finally, the result of the comparation generates an score and a list of the different camps.

  You can find the documentation for matching algorithm on:

  * [`part1/ModelRecord.py`](part1/ModelRecord.py) :
  ```python
  # Line 114
  # From an input record get the possible matches (candidates to be the same record)
  InputRecord.getPosibleMatches()
  # Line 32
  #Compare with other record giving a % int as a result and a list of differences.
  Record.compare()
  ```

  * [`part1/ControllerMatch.py`](part1/ControllerMatch.py) :
  ```python
  # Line 13
  # Function that automatize the population of the table t_matches and the score generation
  ControllerMatch.generateMatches()
  ```

2. Imagine that your database has 10 million sound recordings, do you
think your solution is still valid?

  As 10 million of rows in a table is a huge amount of entries, I think that it will be a need to optimize the compassion algorithm and Postgres configuration.


3. If not, what would you do to improve it?

  Basically optimizing, configuring and improving:
  1. Creating a better and **optimized sql** call to get possible matches. The actual call is not really elegant and I think it could be slow.
  2. **Configuring and designing properly the database**. For example creating a [BRIN index](https://www.postgresql.org/docs/current/brin-intro.html) or looking for other huge database optimizations.
  3. **Optimizing python script** looking for best practices and software design patterns, making tests etc...
