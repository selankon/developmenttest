#!/usr/bin/python
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import config
import sys

"""
Class used to connect to the database
"""

class PostgresDB(object):

    def query(self, query, params):
        return self._db_cur.execute(query, params)

    def __del__(self):
        self._closeConnection

    # Function that closes the connection
    def _closeConnection(self):
        if(self._db_connection):
            self._db_cur.close()
            self._db_connection.close()
            print("PostgreSQL connection is closed")


    def connect(self, conf = config.DB_CONF):
        """
        Connect to a postgres server default database.
        """

        try:
            self._db_connection = psycopg2.connect(user = conf['USER'] ,
                                  password = conf['PASSWORD'],
                                  host = conf['HOST'],
                                  port = conf['PORT'],
                                  database = conf['DATABASE'])

            self._db_cur = self._db_connection.cursor()
            # Uncoment this to get information about the conection
            # self._db_cur.execute("SELECT version();")
            # record = self._db_cur.fetchone()
            print("* You are connected ")
            # Set isolation level able to write on DB
            self._db_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
            sys.exit(1)

    def dbExists(self, db):
        """
        Check if database exist
        :return: Boolean True if exist, boolean False if not exist
        """
        self._db_cur.execute("select count(*) = 0  from pg_catalog.pg_database where datname = '"+db+"' ;")
        return not self._db_cur.fetchone()[0]

    def createDatabase (self, dbName = config.DB_CONF['DATABASE']):
        """
        Create Database function.
        :param dbName: Name from the database to create
        """

        print ("Creating database ", dbName)
        try:
            # If DB exists drop it
            if (self.dbExists(dbName)):
                print("Db already exist, deleting...")
                self._db_cur.execute("DROP DATABASE %s  ;" % dbName)
            # Create DB
            self._db_cur.execute("CREATE DATABASE %s  ;" % dbName)
            config.DB_CONF['DATABASE'] = dbName
            print ("Change to new database" , config.DB_CONF['DATABASE'])
            # Connect to proper database
            self._closeConnection()
            self.connect()
            # Update schema
            print ("Updating schema...")
            self._db_cur.execute(open(config.DEV['SCHEMA'], "r").read())
            print ("Schema updated properly...")

        except (Exception) as error :
            print ("Error while creating database", error)
            sys.exit(1)
