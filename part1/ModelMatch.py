#!/usr/bin/python
import ModelRecord

class Match(object):

    """
    Model object for table t_matches
    """
    def __init__(self, dbConnection,
                id_records = None,
                id_inputs = None,
                match = None,
                diferences = None):
        self.dbConnection = dbConnection
        self.id_records = id_records
        self.id_inputs = id_inputs
        self.match = match
        self.diferences = diferences

    def insert (self):
        sql = "INSERT INTO t_matches (id_records, id_inputs, match, differences) VALUES (%s, %s, %s, %s)"
        val = (self.id_records, self.id_inputs, self.match, self.diferences)
        self.dbConnection.query(sql, val)
