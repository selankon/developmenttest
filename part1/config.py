#!/usr/bin/python

DB_CONF = {
    'USER': "postgres",
    'PASSWORD': "1314",
    'HOST': "127.0.0.1",
    'PORT':  "5432",
    'DATABASE': "devtest",
}

DEV = {
    # Create database on setup.py run
    'CREATEDB': "true",
    'SCHEMA': "../dbschema_PG9.6.sql", # SQL script to execute to create the database
    # Populate database with csv on setup.py run
    'POPULATEDB': "true",
    'RECORDINGS': "../sound_recordings.csv",
    'INPUTREPORT': "../sound_recordings_input_report.csv"
}
