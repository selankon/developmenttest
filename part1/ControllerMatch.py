#!/usr/bin/python
from ModelMatch import Match
import ModelRecord

class ControllerMatch(object):
    """
    Controller for Match
    """

    def __init__(self, dbConnection):
        self.dbConnection = dbConnection

    def generateMatches(self):
        """
        This function get all records from input table, then for each entry get the
        posible matches and process its. The Record.compare() function makes a comparation
        between the input object and stored records, resulting with a dictionari object with a
        score and a list of different camps.
        With the result creates and calls to store a Match object.
        """

        # Get all records from table t_input_records
        records = ModelRecord.InputRecord.getAll(self.dbConnection)
        print("Looking for matches on input table")
        for i in range(len(records)):
            # Get all posible matches from a input
            posibleMatches = records[i].getPosibleMatches()
            for x in range(len(posibleMatches)):
                res = records[i].compare(posibleMatches[x])
                m = Match(self.dbConnection,posibleMatches[x].id, records[i].id, res['score'], res['diferences'] )
                m.insert()
