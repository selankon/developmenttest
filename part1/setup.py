#!/usr/bin/python
from DB import PostgresDB
import config
import csv
import ModelRecord
import ControllerMatch

"""
Script that authomatize database creation, population and run matching
alghoritms, preparing enviorment for first use.
1. If  `config.DEV['CREATEDB'] == "true"` drop DB if exist and create a new one
using configured sql schema.
2. Populate the database if  `config.DEV['POPULATEDB'] == "true"` using .csv files
3. Run matching algoritms between t_input_report and t_sound_recordings, and store
it on table t_matches
"""

def main():

    def importCsv(csvFile, object):
        """
        Function to import a csv into an object
        :param csvFile: path of the csv file
        :param object: Instantiable class of the model that will contain the information
        """
        with open(csvFile) as csvfile:
            # with open(csvFile, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            next(reader)   # skip the first line
            i = 0
            for row in reader:
                r = object(db, "", *row)
                r.insert()

    db = PostgresDB ()
    # Drop and Create database
    if (config.DEV['CREATEDB'] == "true"):
        connection = config.DB_CONF
        connection['DATABASE'] = "postgres"

        # Connect to database
        print ("Starting database creation")
        db.connect(connection)

        # Lets create database
        print ("Creating database...")
        db.createDatabase()
    else:
        db.connect()

    # Populate database with csv data
    if (config.DEV['POPULATEDB'] == "true"):
        print ("Populate database")
        print ("Importing ", config.DEV['RECORDINGS'])
        importCsv(config.DEV['RECORDINGS'], ModelRecord.Record)

        print ("Importing ", config.DEV['INPUTREPORT'])
        importCsv(config.DEV['INPUTREPORT'], ModelRecord.InputRecord)

    # # Run matching comparation algorithms
    print ("Creating match table")
    ctM = ControllerMatch.ControllerMatch(db)
    ctM.generateMatches()

if __name__ == '__main__':
    main()
